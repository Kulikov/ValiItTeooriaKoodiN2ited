package ee.bcs.chooseit.animals;

/**
 * Created by heleen on 28.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length > 0 && args.length % 2 == 0) {
            for (int i = 0; i < args.length - 1; i = i + 2) {
                String species = args[i];
                String actorName = args[i + 1];

                System.out.println("Lets create one " + species + " with name " + actorName);

                switch (species) {
                    case "dolphin":
                        Dolphin dolphin = new Dolphin();
                        dolphin.setGender(Gender.FEMALE);
                        dolphin.defineSpeciesName(actorName);
                        dolphin.moveAhead();
                        break;
                    case "human":
                        Human human = new Human();
                        human.setGender(Gender.MALE);
                        human.defineSpeciesName(actorName);
                        human.moveAhead();
                        break;
                    case "dog":
                        Dog dog = new Dog();
                        dog.setGender(Gender.MALE);
                        dog.defineSpeciesName(actorName);
                        dog.moveAhead();
                        break;
                    default:
                        System.out.println("Don't understand, is it from space?");
                }

                System.out.println("---------");
            }
        }else {
            System.err.println("There is missing pair of species and name");
        }
    }
}
