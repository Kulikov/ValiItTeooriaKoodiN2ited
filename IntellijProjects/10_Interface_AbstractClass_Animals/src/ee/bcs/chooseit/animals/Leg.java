package ee.bcs.chooseit.animals;

/**
 * Created by heleen on 28.06.2017.
 */
public enum Leg {
    LEFT("This is left leg"), RIGHT ("This is right leg");
    private final String description;

    private Leg(String d) {
        description = d;
    }
    public String getDescription() {
        return description;
    }
}
