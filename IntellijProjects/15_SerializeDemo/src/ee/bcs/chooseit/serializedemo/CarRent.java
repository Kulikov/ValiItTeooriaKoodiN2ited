/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.serializedemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author heleen
 */
public class CarRent implements Serializable {

    private List<Car> carsAvailableForRent = new ArrayList<>();
    private static final String CAR_RENT_PARK_NAME = "Tallinn Car Rent";

    public CarRent(Car... car) {
        carsAvailableForRent.addAll(Arrays.asList(car));
    }

    public void setCarsAvailableForRent(List<Car> carsAvailableForRent) {
        this.carsAvailableForRent = carsAvailableForRent;
    }

    @Override
    public String toString() {
        return CAR_RENT_PARK_NAME + ": CarRent{\n" + "carsAvailableForRent=" + carsAvailableForRent + '}';
    }

}
