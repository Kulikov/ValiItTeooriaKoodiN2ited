package ee.bcs.chooseit.collectionsintro;

import java.util.Objects;

public class Car implements Comparable<Car> {
    private final String modelName;
    private final int productionYear;

    public Car(String modelName, int productionYear) {
        this.modelName = modelName;
        this.productionYear = productionYear;
    }

    public String getModelName() {
        return this.modelName;
    }

    public int getProductionYear() {
        return this.productionYear;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Car other = (Car) obj;
        if (this.productionYear != other.productionYear) {
            return false;
        }
        return Objects.equals(this.modelName, other.modelName);
    }

    @Override
    public String toString() {
        return "Car{" + "productionYear=" + productionYear + ", modelName=" + modelName + '}';
    }

    @Override
    public int compareTo(Car otherCar) {
        if (this.equals(otherCar)) {
            return 0;
        } else if (this.productionYear <= otherCar.getProductionYear()) {
            return -1;
        } else if (this.modelName.compareTo(otherCar.getModelName()) <= -1) {
            return -1;
        } else {
            return 1;
        }
    }
}
