package valiit.basicoo;

public class App 
{
    public static void main( String[] args )
    {
        Vehicle myBus = new Vehicle();
        myBus.drivers.add("Mati");
        myBus.drivers.add("Kati");
        myBus.drivers.add("Malle");
        myBus.drivers.add("Pille");
        myBus.printDriversCount();
        myBus.move();
        
        Vehicle myBoat = new Boat();
        myBoat.drivers.add("Peeter");
        myBoat.drivers.add("Paul");
        myBoat.drivers.add("Kristo");
        myBoat.drivers.add("Kaidi");
        myBoat.drivers.add("Malle");
        myBoat.printDriversCount();
        myBoat.move();
        
        Plane myPlane = new Plane();
        myPlane.drivers.add("Peeter");
        myPlane.drivers.add("Malle");
        myPlane.printDriversCount();
        myPlane.move();
    }
}
