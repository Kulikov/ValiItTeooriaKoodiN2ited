package valiit.basicoo;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {

	public List<String> drivers = new ArrayList<String>();
	
	public void printDriversCount() {
		int count = 0;
		for(String driver : this.drivers) {
			if (this.isQualifiedForDriving(driver)) {
				count += 1;
			}
		}
		System.out.println("Drivers count for the vehicle is " + count);
	}
	
	public void move() {
		System.out.println("The vehicle is rolling...");
	}
	
	private boolean isQualifiedForDriving(String driver) {
		return !driver.toLowerCase().startsWith("m");
	}
}
