package valiit.basicoo;

public class Boat extends Vehicle {

	@Override
	public void move() {
		System.out.println("The boat is swimming...");
	}
}
