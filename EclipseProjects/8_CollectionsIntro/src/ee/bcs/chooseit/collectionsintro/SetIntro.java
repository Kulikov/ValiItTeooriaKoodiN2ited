package ee.bcs.chooseit.collectionsintro;

import java.util.Set;
import java.util.TreeSet;

public class SetIntro {
	public static void main(String[] args) {
		simpleSetExample();
		comparatorExample();
	}

	private static void simpleSetExample() {
		Set<String> simpleSet = new TreeSet<>();
		simpleSet.add("text");
		simpleSet.add("alphabet");
		simpleSet.add("horse");
		simpleSet.add("text");
		simpleSet.add("vague");
		simpleSet.forEach(t -> {
			System.out.println(t);
		});
		System.out.println("Set size is " + simpleSet.size() + "\n");
	}

	private static void comparatorExample() {
		Car toyota2016 = new Car("Toyota", 2016);
		Car toyota2010 = new Car("Toyota", 2010);
		Car mazda2010 = new Car("Mazda", 2010);
		Car mazda1999 = new Car("Mazda", 1999);
		Car mercedes2005 = new Car("Mercedes", 2005);

		Set<Car> carSet = new TreeSet<>();
		carSet.add(toyota2016);
		carSet.add(toyota2010);
		carSet.add(mazda2010);
		carSet.add(mazda1999);
		carSet.add(mercedes2005);
		carSet.add(mazda1999);

		carSet.forEach(car -> {
			System.out.println(car);
		});
	}
}
