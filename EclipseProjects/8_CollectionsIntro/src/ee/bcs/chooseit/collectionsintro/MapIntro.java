package ee.bcs.chooseit.collectionsintro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapIntro {
	public static void main(String[] args) {
		Map<String, List<String>> departmentEmployees = new HashMap<>();
		List<String> devTeam = new ArrayList<>();
		devTeam.add("Mari Maasikas");
		devTeam.add("Jüri Kuusk");
		departmentEmployees.put("development", devTeam);
		List<String> financeTeam = new ArrayList<>();
		financeTeam.add("Kati Kivi");
		departmentEmployees.put("finance", financeTeam);
		List<String> adminTeam = new ArrayList<>();
		adminTeam.add("Mati Kadakas");
		departmentEmployees.put("admin", adminTeam);
		// Get list of current employees in department finance and add one new
		// employee there
		departmentEmployees.get("finance").add("Kadri Kaasik");

		for (String key : departmentEmployees.keySet()) {
			System.out.print("In department " + key + " there are working: ");
			for (String employee : departmentEmployees.get(key)) {
				System.out.print(employee + "; ");
			}
			System.out.println();
		}
		
		// Same printing using lambda
		departmentEmployees.keySet().stream().map((key) -> {
			System.out.print("In departement " + key + " there are working: ");
			return key;
		}).map((key) -> {
			departmentEmployees.get(key).forEach((employee) -> {
				System.out.print(employee + "; ");
			});
			return key;
		}).forEachOrdered((_item) -> {
			System.out.println();
		});
		
		// Same printing using another version in lambda
		departmentEmployees.forEach((department, employees) -> {
			System.out.print("In department " + department + " there are working: ");
			employees.forEach(employee -> {
				System.out.print(employee + "; ");
			});
			System.out.println();
		});
	}
}
