package ee.bcs.chooseit.threading;

/**
 *
 * @author heleen
 */
public class ExampleRunnableNoSync implements Runnable {

    static int i = 0;

    @Override
    public void run() {
        i++;
        System.out.println(Thread.currentThread().getName() + " i = " + i);
    }
}
