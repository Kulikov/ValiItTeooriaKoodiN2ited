package ee.bcs.chooseit;

/**
 * 
 * @author heleen
 *
 */
public class VarargsDemo {
	public static int getSum(int... valueToSum) {
		int sum = 0;
		for (int value : valueToSum) {
			sum = sum + value;
		}
		return sum;
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		System.out.println("2 + 5 + 10 + 18 = " + VarargsDemo.getSum(2, 5, 10, 18));
		System.out.println("2 + 5 + 10 = " + VarargsDemo.getSum(2, 5, 10));
		System.out.println("2 + 5 = " + VarargsDemo.getSum(2, 5));
		System.out.println("2 = " + VarargsDemo.getSum(2));
		System.out.println(" = " + VarargsDemo.getSum());
	}
}
