package ee.bcs.chooseit.visibility;

/**
 * 
 * @author heleen
 *
 */
public class Visibility {
	private String privateClassVariable = "Private class variable is visible only inside the class";
	String defaultClassVariable = "Default class variable is visible only in same package ee.bcs.chooseit.visibility";
	protected String protectedClassVariable = "Protected class variable is also visible in package ee.bcs.chooseit.visbility.sub class VisibilitySub";
	public String publicClassVariable = "Public class variable is visible within whole project";

	public Visibility() {
	}

	public Visibility(String varjatudMuutujaUusVaartus) {
		this.privateClassVariable = varjatudMuutujaUusVaartus;
	}

	public void testVariablesInVisibility() {
		privateClassVariable = privateClassVariable.concat(" and as it is not final, you can modify it's value");
		System.out.println(privateClassVariable);
		System.out.println(defaultClassVariable);
		System.out.println(protectedClassVariable);
		System.out.println(this.protectedClassVariable);
		System.out.println(publicClassVariable);
	}

	public final void finalMethod() {
		System.out.println("This is a final method");
	}
}
