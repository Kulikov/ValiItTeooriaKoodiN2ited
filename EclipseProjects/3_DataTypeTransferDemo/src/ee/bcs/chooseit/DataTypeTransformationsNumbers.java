package ee.bcs.chooseit;

/**
 * 
 * @author heleen
 *
 */
public class DataTypeTransformationsNumbers {
	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		Integer integerValue = Integer.parseInt("10");
		System.out.println(integerValue);
		Double doubleValue = integerValue.doubleValue();
		System.out.println(doubleValue);

		doubleValue = doubleValue + 0.5;
		System.out.println(doubleValue);
		Integer integerValueFromDouble = doubleValue.intValue();
		System.out.println(integerValueFromDouble);
	}
}
