package ee.bcs.person;

public class IncorrectEmployeeDetails extends Exception {
	private static final long serialVersionUID = -5126512475726464277L;

	public IncorrectEmployeeDetails() {
		super();
	}
	public IncorrectEmployeeDetails(String message) {
		super(message);
	}

}
