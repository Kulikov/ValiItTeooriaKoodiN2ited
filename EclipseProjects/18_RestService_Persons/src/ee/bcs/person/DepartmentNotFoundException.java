package ee.bcs.person;

public class DepartmentNotFoundException extends Exception {
	private static final long serialVersionUID = -5031710025869653188L;

	public DepartmentNotFoundException() {
		super();
	}

	public DepartmentNotFoundException(String message) {
		super(message);
	}
}
