package ee.bcs.person;

/**
 *
 * @author heleen
 */
public class Employee {

	private String firstName;
	private String lastName;
	private String personalID;
	private Gender gender;

	public Employee() throws IncorrectEmployeeDetails {
		this(null, null, null);
	}

	public Employee(String firstName, String lastName, String personalID) throws IncorrectEmployeeDetails {
		this.firstName = firstName;
		this.lastName = lastName;
		if (personalID == null || personalID.matches("\\d{11}")) {
			this.personalID = personalID;
		} else {
			throw new IncorrectEmployeeDetails("Incorrect personalId");
		}

		if (personalID != null) {
			setGenderFromPersonalID();
		}
	}

	public String getFirstName() {
		return this.firstName;
	}

	public Employee setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return this.lastName;
	}

	public Employee setLastName(String lastName) {
		this.lastName =lastName;
		return this;
	}

	public String getPersonalID() {
		return this.personalID;
	}

	public Employee setPersonalId(String personalId) {
		this.personalID = personalId;
		setGenderFromPersonalID();
		return this;
	}

	public Gender getGender() {
		return this.gender;
	}

	@Override
	public String toString() {
		return "Employee{" + "firstName=" + firstName + ", lastName=" + lastName + ", personalID=" + personalID
				+ ", gender=" + gender + '}';
	}

	public void setGenderFromPersonalID() {
		this.gender = isMale() ? Gender.MALE : Gender.FEMALE;
	}

	private boolean isMale() {
		return this.personalID.charAt(0) % 2 != 0;
	}

}
