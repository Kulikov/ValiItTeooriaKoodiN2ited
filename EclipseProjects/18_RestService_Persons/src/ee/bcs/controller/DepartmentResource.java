package ee.bcs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.person.Department;
import ee.bcs.person.DepartmentNotFoundException;
import ee.bcs.person.Employee;

@Path("/departments")
public class DepartmentResource {
	private static List<Department> departments = new ArrayList<>();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Department> getDepartmentsList() {
		return departments;
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Department getDepartmentById(int departmentId) {
		return findDepartment(departmentId);
	}

	private Department findDepartment(int departmentId) {
		for (Department department : departments) {
			if(department.getDepartmentId() == departmentId) {
				return department;
			}
		}
		return null;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Department addDepartment(Department department) {
		departments.add(department);
		return department;
	}
	
	@Path("/{id}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Department addEmployeeToDepartment(@PathParam("id") int departmentId, Employee employee) throws DepartmentNotFoundException {
		Department department = findDepartment(departmentId);
		if(department != null){
			department.getDepartmentEmployees().add(employee);
		}else {
			throw new DepartmentNotFoundException("Department not found");
		}
		
		return department;
	}
	
}
