package ee.bcs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ee.bcs.person.Employee;
import ee.bcs.person.IncorrectEmployeeDetails;

@Path("/employees")
public class EmployeeResource {
	static List<Employee> employees = new ArrayList<>();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployees() {
		return employees;
	}

	@Path("/{personalId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployeeById(@PathParam("personalId") String personalId) {		
		return findEmployeeById(personalId);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Employee addEmployee(Employee person) {
		employees.add(person);
		return person;
	}

	@DELETE
	public void deleteEmployeeByLastName(@QueryParam("personalId") String personalId) {
		employees.removeIf(p -> p.getPersonalID() == personalId);
	}

	@Path("/{id}")
	@DELETE
	public void deleteEmployeeByFirstName(@PathParam("personalId") String personalId) {
		employees.removeIf(p -> p.getPersonalID() == personalId);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Employee modifyEmployee(Employee employee) throws IncorrectEmployeeDetails {
		boolean isEmployeeFound = false;
		for (Employee emp : employees) {
			if (emp.getLastName().equals(employee.getLastName())) {
				emp.setFirstName(employee.getFirstName());
				isEmployeeFound = true;
			}
		}
		if (!isEmployeeFound) {
			throw new IncorrectEmployeeDetails("Employee was not found");
		}

		return employee;
	}

	@PATCH
	@Produces(MediaType.APPLICATION_JSON)
	public Employee modifyEmployeePartially(Employee employee) {
		Employee modifiedEmployee = null;
		if (employee.getPersonalID() != null) {
			modifiedEmployee = findEmployeeById(employee.getPersonalID());

			if (modifiedEmployee != null) {
				if (employee.getFirstName() != null) {
					modifiedEmployee.setFirstName(employee.getFirstName());
				}
				if (employee.getLastName() != null) {
					modifiedEmployee.setLastName(employee.getLastName());
				}
			} else {
				modifiedEmployee = employee;
				employees.add(modifiedEmployee);
			}
		}

		return modifiedEmployee;
	}

	private Employee findEmployeeById(String personalID) {
		Employee modifiedEmployee = null;
		for (Employee emp : employees) {
			if (emp.getPersonalID().equals(personalID)) {
				modifiedEmployee = emp;
			}
		}
		return modifiedEmployee;
	}
}
