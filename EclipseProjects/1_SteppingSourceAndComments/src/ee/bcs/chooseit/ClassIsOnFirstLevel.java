package ee.bcs.chooseit;

/**
 * This class is for demonstrating how to format source
 *
 * @author heleen - author of this class, can be also the name of company
 * @version 1.0 - source code version, e.g important for services
 */
public class ClassIsOnFirstLevel {
	// class variables and methods are one level lower than class, mostly
	// intended by 1xtab or 4x space, they are contained within curly braces
	// that
	// are located on rows 9 and 61

	/**
	 * you can add javadoc comment to the variable for documenting. Current
	 * variable is constant for limiting length of some variable written in
	 * upper case, visibility is limited to private, so it is visible in this
	 * class only.
	 */
	private final int WORD_MAX_LENGTH = 50;
	private int thisVariableIsOneLevelBelow = 20;
	// you can declare variable without value, but it is not recommended as it
	// may cause exceptions and incorrect behaviour of application
	private int numberWithoutValue;
	private int numberWithValue = 0; // it is recommended to give a value

	/*
	 * Here is code commented out with block comment, compiler will ignore it
	 * private String variableForComment="test";
	 * 
	 * private void methodForComment(){ variableForComment ="test2"; }
	 */

	/**
	 * Method reads in variable addable and it is positive number, it will add
	 * it to the variable thisVariableIsOneLevelBelow
	 *
	 * @param addable
	 *            - variable value that will be summed up to the
	 *            thisVariableIsOneLevelBelow, if it is positive value
	 * @return thisVariableIsOneLevelBelow + addable value
	 */
	private int classMethodIsOneLevelBelowTheClass(int addable) {
		// if is already on third level as it is inside the method
        if (addable > 0) {
            // following command is on the fourth level as it is inside the if-statement
            return thisVariableIsOneLevelBelow + addable;
        }
        return thisVariableIsOneLevelBelow;
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		// main method is also class-method that is one step below class
		ClassIsOnFirstLevel k = new ClassIsOnFirstLevel();
		System.out.println(k.classMethodIsOneLevelBelowTheClass(2));
		// TODO - this style of comment references to something that should be
		// done in future, for example following row should be deleted at some
		// point
		System.out.println('A' | 'B');
	}
}
