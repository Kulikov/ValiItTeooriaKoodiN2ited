package ee.bcs.demo;

public class SimpleService 
{	
	public int add5(int number) {
		return number + 5;
	}
	
	public String concatenateStrings(String string1, String string2) {
		return string1 + string2;
	}
	
	public static void printNumbersDesc(int number) {
		System.out.println(number);
		if (number > 1) {
			printNumbersDesc(--number);
		}
	}
	
	public static int add(int x, int y) { return x + y;}
	public static int add(int x, int y, int z) { return x + y + z;}
	public static double add(double x, double y) { return x + y;}
	public static double add(double x, double y, double z) { return x + y + z;}

	public int multiplyWith5(int i) {
		return i * 5;
	}

}
