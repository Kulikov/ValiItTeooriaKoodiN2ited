package ee.bcs.chooseit.localedemo;

import java.util.Date;

/**
 *
 * @author heleen
 */
public class Main {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		/**
		 * !!! resource folder has to be included into class path: Right click on
		 * project -> select Build path -> Configure build path -> move to tab
		 * Source -> Add Folder -> select resource folder
		 */
		Date today = new Date();
		Double money = 1000000.0;
		LocaleDemo localeDemo = new LocaleDemo();

		localeDemo.printHello("Heleen");
		localeDemo.printBye("Heleen");
		localeDemo.showDate(today);
		localeDemo.showMoneyWithCurrency(money);

		localeDemo.setLocaleToEstonia();
		localeDemo.printHello("Heleen");
		localeDemo.printBye("Heleen");
		localeDemo.showDate(today);
		localeDemo.showMoneyWithCurrency(money);

		localeDemo.setLocaleToUS();
		localeDemo.printHello("Heleen");
		localeDemo.printBye("Heleen");
		localeDemo.showDate(today);
		localeDemo.showMoneyWithCurrency(money);

		localeDemo.setLocaleToFrance();
		localeDemo.printHello("Heleen");
		localeDemo.printBye("Heleen");
		localeDemo.showDate(today);
		localeDemo.showMoneyWithCurrency(money);

		localeDemo.setLocaleToDefault();
		localeDemo.printHello("Heleen");
		localeDemo.printBye("Heleen");
		localeDemo.showDate(today);
		localeDemo.showMoneyWithCurrency(money);
	}

}
