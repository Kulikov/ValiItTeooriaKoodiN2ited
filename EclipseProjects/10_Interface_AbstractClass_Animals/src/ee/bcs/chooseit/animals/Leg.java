package ee.bcs.chooseit.animals;

public enum Leg {
	LEFT("This is left leg"), RIGHT ("This is right leg");
    private final String description;
    
    private Leg(String d) {
        description = d;
    }
    public String getDescription() {
        return description;
    }
}
