package ee.bcs.chooseit.animals;

public enum Gender {
	MALE, FEMALE;
}
