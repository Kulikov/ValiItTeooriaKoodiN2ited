package ee.bcs.chooseit.employeemanagement.view;

import ee.bcs.chooseit.employeemanagement.EmployeesManagementMain;
import ee.bcs.chooseit.employeemanagement.model.Employee;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 *
 * @author heleen
 */
public class EmployeesOverviewController {
    
    @FXML
    private TableView<Employee> employeesTable;
    @FXML
    private TableColumn<Employee, String> firstNameCol;
    @FXML
    private TableColumn<Employee, String> lastNameCol;
    @FXML
    private TableColumn<Employee, String> personalIdCol;
    @FXML
    private TableColumn<Employee, String> genderCol;
    
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label personalIdLabel;
    @FXML
    private Label genderLabel;
    
    private EmployeesManagementMain employeesManagement;
    
    public EmployeesOverviewController() {
    }
    
    @FXML
    public void initialize() {
        firstNameCol.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameCol.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        personalIdCol.setCellValueFactory(cellData -> cellData.getValue().personalIDProperty());
        genderCol.setCellValueFactory(cellData -> cellData.getValue().genderProperty().asString());

        // Clear person details.
        showEmployeeDetails(null);

        // Listen for selection changes and show the person details when changed.
        employeesTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showEmployeeDetails(newValue));
    }
    
    public void setEmployeeManagement(EmployeesManagementMain employeeManagement) {
        this.employeesManagement = employeeManagement;

        // Add observable list data to the table
        employeesTable.setItems(employeeManagement.getEmployeeData());
    }
    
    private void showEmployeeDetails(Employee employee) {
        if (employee != null) {
            firstNameLabel.setText(employee.getFirstName());
            lastNameLabel.setText(employee.getLastName());
            personalIdLabel.setText(employee.getPersonalID());            
            genderLabel.setText(employee.getGender().toString());
        } else {
            firstNameLabel.setText("");
            lastNameLabel.setText("");
            personalIdLabel.setText("");
            genderLabel.setText("");
        }
    }
    
    @FXML
    private void handleDeleteEmployee() {
        int selectedIndex = employeesTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            employeesTable.getItems().remove(selectedIndex);
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(employeesManagement.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Employee Selected");
            alert.setContentText("Please select a employee in the table.");
            
            alert.showAndWait();
        }
    }
    
    @FXML
    private void handleNewEmployee() throws Exception {
        Employee tempEmployee = new Employee();
        boolean okClicked = employeesManagement.showEmployeeEditDialog(tempEmployee);
        if (okClicked) {
            employeesManagement.getEmployeeData().add(tempEmployee);
        }
    }
    
    @FXML
    private void handleEditEmployee() {
        Employee selectedEmployee = employeesTable.getSelectionModel().getSelectedItem();
        if (selectedEmployee != null) {
            boolean okClicked = employeesManagement.showEmployeeEditDialog(selectedEmployee);
            if (okClicked) {
                showEmployeeDetails(selectedEmployee);
            }
            
        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(employeesManagement.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Person Selected");
            alert.setContentText("Please select a person in the table.");
            
            alert.showAndWait();
        }
    }
}
