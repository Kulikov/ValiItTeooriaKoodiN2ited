package valiit.threadapp;

public class MyThread extends Thread {

	private String id = null;
	private Object lock = null;
	
	public MyThread(String id, Object lock) {
		this.id = id;
		this.lock = lock;
	}
	
	@Override
	public void run() {
		easySleep(this.id);
		System.out.println("I am going to run now: " + this.id);
	}
	
	private static synchronized void easySleep(String id) {
//		synchronized(this.lock) {
			try {
				long seconds = (long)(Math.random() * 10 * 1000);
				System.out.println(String.format("Thread %s: I am going to sleep for %s seconds", id, seconds / 1000.0));
				Thread.sleep(seconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//		}
	}
}
