package valiit.threadapp;

public class App 
{
    public static void main( String[] args )
    {
    	Object lock = new Object();
    	
        Thread myThreadObj1 = new MyThread("1", lock);
        Thread myThreadObj2 = new MyThread("2", lock);
        Thread myThreadObj3 = new MyThread("3", lock);
        Thread myThreadObj4 = new MyThread("4", lock);
//        Thread myThreadObj5 = new MyThread("5");
//        Thread myThreadObj6 = new MyThread("6");
//        Runnable myRunnableObj1 = new MyRunnable("R1");
        
        myThreadObj1.start();
        myThreadObj2.start();
        myThreadObj3.start();
        myThreadObj4.start();
//        myThreadObj5.run();
//        myThreadObj6.run();
//        myRunnableObj1.run();
        
        System.out.println("Main meetod lõpetab tegevuse nüüd!");
    }
}
