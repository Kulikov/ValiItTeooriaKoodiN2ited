package ee.bcs.chooseit;

/**
 * 
 * @author heleen
 *
 */
public class StringSplit {

	public static void main(String[] args) {
		String textContainingListOfNames = "This is a list of city names: Tallinn, Tartu, Pärnu., Kuressaare.";
		String[] textWithNamesOnly = textContainingListOfNames.split(":");
		String[] arrayOfCityNames = textWithNamesOnly[1].split(",");
		for (String cityName : arrayOfCityNames) {
			System.out.println("linn:" + cityName.replace(".", ""));
		}
	}
}
