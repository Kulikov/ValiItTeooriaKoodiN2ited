package ee.bcs.chooseit;

/**
 * StringBuilder is asynchronous
 * 
 * @author heleen
 *
 */
public class StringBuilderDemo {
	public static void main(String[] args) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("This is a first sentence. \n");
		stringBuilder.append("This is second sentence added with append. \n");
		int thirdBeginning = stringBuilder.length();
		stringBuilder.insert(thirdBeginning,
				"This is third sentnece added by using insert, is added to the end of string buffer. \n");
		String neljasLause = "This is fourth sentence that goes before third sentence as using same position thirdBeginning. \n";
		stringBuilder.insert(thirdBeginning, neljasLause);
		System.out.println(stringBuilder);

		// lets remove fourth sentence, that was added to third start position
		stringBuilder.delete(thirdBeginning, thirdBeginning + neljasLause.length());
		System.out.println(stringBuilder);
	}
}
