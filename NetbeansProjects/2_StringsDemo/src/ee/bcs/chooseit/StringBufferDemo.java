/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit;

/**
 * StringBuffer is asynchronous
 * @author heleen
 *
 */
public class StringBufferDemo {
	
    public static void main(String[] args) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("This is a first sentence. \n");
        stringBuffer.append("This is second sentence added with append. \n");
        int thirdBeginning = stringBuffer.length();
        stringBuffer.insert(thirdBeginning, "This is third sentnece added by using insert, is added to the end of string buffer. \n");
        String neljasLause = "This is fourt sentence that goes before third sentence as using same position thirdBeginning. \n";
        stringBuffer.insert(thirdBeginning, neljasLause);
        System.out.println(stringBuffer);

        // lets remove fourth sentence, that was added to third start position
        stringBuffer.delete(thirdBeginning, thirdBeginning + neljasLause.length());
        System.out.println(stringBuffer);
    }
}
