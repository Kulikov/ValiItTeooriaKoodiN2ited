package ee.bcs.chooseit.visibility.sub;

import ee.bcs.chooseit.visibility.Visibility;

/**
 *
 * @author heleen
 */
public class VisibilitySubClassInOtherPackage extends Visibility {

    @Override
    public void testVariablesInVisibility() {
        Visibility visibility = new Visibility();
        // System.out.println(visibility.privateClassVariable);
        // System.out.println(visibility.defaultClassVariable);

        /**
         * as the class is in other package
         */
        // System.out.println(visibility.protectedClassVariable);

        // Due to the inheritance class VisibilitySubClassInOtherPackage has
        // variable protectedClassVariable, but this doesn't belong to
        // previously defined visibility
        System.out.println(this.protectedClassVariable);
        System.out.println(visibility.publicClassVariable);
    }
}
