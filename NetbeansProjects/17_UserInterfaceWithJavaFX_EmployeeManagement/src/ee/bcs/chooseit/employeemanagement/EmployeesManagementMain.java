/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.employeemanagement;

import ee.bcs.chooseit.employeemanagement.model.Employee;
import ee.bcs.chooseit.employeemanagement.view.EmployeeEditDialogController;
import ee.bcs.chooseit.employeemanagement.view.EmployeesOverviewController;
import java.io.IOException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author heleen
 */
public class EmployeesManagementMain extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private final ObservableList<Employee> employeesObservable;

    public EmployeesManagementMain() throws Exception {
        this.employeesObservable = FXCollections.observableArrayList();
        this.employeesObservable.add(new Employee("Mari", "Maasikas", "48705310221"));
        this.employeesObservable.add(new Employee("Mihkel", "Juurikas", "36709300221"));
    }

    public ObservableList<Employee> getEmployeeData() {
        return employeesObservable;
    }

    public void showEmployeesOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EmployeesManagementMain.class.getResource("view/EmployeesOverview.fxml"));
            AnchorPane employeesOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(employeesOverview);

            // Give the controller access to the main app.
            EmployeesOverviewController controller = loader.getController();
            controller.setEmployeeManagement(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showEmployeeEditDialog(Employee employee) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EmployeesManagementMain.class.getResource("view/EmployeeEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Employee");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            EmployeeEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setEmployee(employee);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EmployeesManagementMain.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Employees Management");

        initRootLayout();

        showEmployeesOverview();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
