/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.animals;

/**
 *
 * @author heleen
 */
public class Dolphin extends Mammal implements ISeaMammal{

    @Override
    public void moveAhead() {
        swimToSurface();
        breathe();
        dive();
        swimAhead();
    }

    @Override
    public void swimAhead() {
        System.out.println("Dolphin swam ahead.");
    }

    @Override
    public void dive() {
        System.out.println("Dolphin dove.");
    }

    private void breathe() {
        System.out.println("Dolphin breathed.");
    }

    private void swimToSurface() {
        System.out.println("Dolphin swam to surface.");
    }
}
