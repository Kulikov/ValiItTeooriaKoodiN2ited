package ee.bcs.chooseit.employee;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author heleen
 */
public class Employee extends Person {

    private BigDecimal salary;
    private String name;

    public Employee(int age, String name, BigDecimal salary) {
        super(age);
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getSalary() {
        return this.salary;
    }

    public Employee setSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + ", salary=" + salary + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.getId());
        hash = 29 * hash + Objects.hashCode(this.getAge());
        hash = 29 * hash + Objects.hashCode(this.salary);
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        return this.getId() == other.getId()
                && this.getAge() == other.getAge()
                && this.name.equals(other.name)
                && this.salary.equals(other.salary);
    }
}
