/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.employee;

import java.math.BigDecimal;

/**
 *
 * @author heleen
 */
public class Main {

    private static void changeSalary(Employee emp, BigDecimal salary) {
        emp.setSalary(emp.getSalary().add(salary));
    }

    private static void changeSalary(Employee emp) {
        //emp = new Employee();
        changeSalary(emp, BigDecimal.valueOf(100));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Employee employee = new Employee(25, "Kaspar", BigDecimal.valueOf(1_000.0));
        employee.setSalary(BigDecimal.valueOf(1_000.0));
        System.out.println("Initial salary is " + employee);
        //Alternatiiv kahele eelnevale reale, on kasutada lambdat
        System.out.println("Initial salary is with alternative " + employee);
        changeSalary(employee);
        System.out.println("New salary: " + employee);

        changeSalary(employee, BigDecimal.TEN);
        System.out.println("Overload salary: " + employee);

        Employee employee2 = new Employee(22, "Mari", BigDecimal.valueOf(1000));
        System.out.println(employee2);

        Manager manager = new Manager(25, "Kati", BigDecimal.valueOf(1050), "Tesla");
        manager.setSalary(BigDecimal.TEN);
        manager.setDepartment("PKH");
        System.out.println(manager);

        Employee managerEmp = new Manager(25, "Mati", BigDecimal.valueOf(1050), "Tesla");
        managerEmp.setSalary(BigDecimal.TEN);
        System.out.println(managerEmp);
        //managerEmp.setDepartment("PKH");

        //Manager employeeMan = new Employee(30, "Mari", BigDecimal.ZERO);
        modifyDepartment(manager, "Einstein");
        System.out.println(manager);

        modifyDepartment(managerEmp, "Newton");
        System.out.println(managerEmp);

        modifyDepartment(employee2, "Bell");
        System.out.println(employee2);
    }

    private static void modifyDepartment(Employee employee, String newDepartmentName) {
        if (employee instanceof Manager) {
            System.out.println("Department will be changed from " + ((Manager) employee).getDepartment()
                    + " to " + newDepartmentName);
            ((Manager) employee).setDepartment(newDepartmentName);
        }else{
            System.out.println("No changes will be done");
        }
    }
}
